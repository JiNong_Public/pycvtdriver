# pycvtdriver
cvtdriver를 Python 을 통해서도 다룰 수 있도록 하는 모듈

## 소개
본 프로젝트는 (사)한국농식품ICT융복합산업협회와 서울대학교에서 개발한 스마트팜 표준 컨버터에 활용되는 드라이버를 Python에서도 접근할 수 있도록 해주는 모듈을 개발하기 위한 것이다. 

CvtDriver와 관련된 내용은 [드라이버 저장소](https://github.com/ebio-snu/cvtdriver)를 참고 한다.  

## build 방법
```
# mkdir build
# cd build
# cmake ..
# make
```

## 사용법
```
# python
Python 2.7.15rc1 (default, Nov 12 2018, 14:31:15) 
[GCC 7.3.0] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> import pycvt
>>> p = pycvt.PyCvtDriver("../conf/pycvtdriver.conf")
```

## 설정파일
설정파일은 CvtDriver 의 설정파일과 유사하다.
```
{
  "driver": "../cvtdriver/lib/libdssample.so",
  "option": {
    "port": "/dev/ttyUSB0",
    "baudrate": 115200,
    "_sensors" : [{
      "id" : "10",
      "type" : "DT_SEN_HUMIDITY",
      "section" : "DL_DEFAULT_PLANTZONE",
      "target" : "DO_ENV_ATMOSPHERE",
      "status" : "DS_SEN_NORMAL",
      "unit" : "OU_PERCENT"
    }, {
      "id" : "11",
      "type" : "DT_SEN_TEMPERATURE",
      "section" : "DL_DEFAULT_PLANTZONE",
      "target" : "DO_ENV_ATMOSPHERE",
      "status" : "DS_SEN_NORMAL",
      "unit" : "OU_CELSIUS"
    }],
    "_switches" : [{
      "id" : "30",
      "type" : "DT_SWC_FAN",
      "section" : "DL_DEFAULT_PLANTZONE",
      "target" : "DO_EQUIPMENT",
      "status" : "DS_SWC_OFF"
    }, {
      "id" : "31",
      "type" : "DT_SWC_FAN",
      "section" : "DL_DEFAULT_PLANTZONE",
      "target" : "DO_EQUIPMENT",
      "status" : "DS_SWC_OFF"
    }],
    "_motors" : [{
      "id" : "20",
      "type" : "DT_MOT_SIDEWINDOW",
      "section" : "DL_DEFAULT_PLANTZONE",
      "target" : "DO_EQUIPMENT",
      "status" : "DS_MOT_STOP"
    }, {
      "id" : "21",
      "type" : "DT_MOT_SIDEWINDOW",
      "section" : "DL_DEFAULT_PLANTZONE",
      "target" : "DO_EQUIPMENT",
      "status" : "DS_MOT_STOP"
    }]
 }
}
```

## 개발관련 공통사항
* 라이브러리
  * [google glog](https://github.com/google/glog) : Google 에서 공개한 로깅라이브러리
  * [jsoncons](https://danielaparker.github.io/jsoncons/) : JSON 라이브러리
  * [Boost](http://www.boost.org/) : [asio](https://think-async.com/)와 [dll](http://www.boost.org/doc/libs/1_65_1/doc/html/boost_dll.html)을 주로 사용
  * [pybind11](https://github.com/pybind/pybind11) : C++ 과 Python 사이의 바인딩을 수행하는 라이브러리
  * 개별 드라이버 개발을 위해 필요한 라이브러리를 사용할 수 있다.

* 개발환경
  * g++ : 기본 설정으로 g++을 사용. VC에서 테스트되지는 않았지만 큰 문제는 없을것으로 기대.
  * cmake : 빌드환경

* 개발문서
  * [doxygen](http://www.stack.nl/~dimitri/doxygen/) : 코드 문서화를 위해 활용

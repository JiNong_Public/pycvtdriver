/**
 Copyright © 2018 JiNong Inc. 
 All Rights Reserved.

 @file pycvtdriver.h
 @date 2019-04-04
 @author Kim, JoonYong <joonyong.jinong@gmail.com>

 This file loads a cvtdriver for pybind.
 refer from: https://gitlab.com/ebio-snu/stdcvt
*/

#ifndef _PY_CVT_DRIVER_
#define _PY_CVT_DRIVER_

#include <ctime>
#include <iostream>

#include <boost/dll/import.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <jsoncons/json.hpp>
#include <jsoncons_ext/jsonpath/json_query.hpp>

#include <glog/logging.h>

#include <pybind11/pybind11.h>

#include <cvtdevice.h>
#include <cvtoption.h>
#include <cvtdriver.h>

namespace dll = boost::dll;

namespace pycvt {

/*
 @brief PyCvtDriver is a class to load a driver shared library.
*/
class PyCvtDriver {
private:
    boost::shared_ptr<CvtDriver> _driver;
    boost::asio::io_service _iosvc;


public:
    /**
     새로운 드라이버를 생성한다.
     @param modelcode 모델코드
     @param apispec API 버전
    */
    PyCvtDriver(string optfile) {
        ifstream is(optfile);
        json config = json::parse(is);
        string driverfile = config["driver"].as<string>();

        CvtOption option(config["option"]);
        option.setobject (CVT_OPTION_ASIO_SERVICE, &_iosvc);

        LOG(INFO) << "Loading Driver : " << config["driver"].as<string>();

        _driver = dll::import<CvtDriver>(driverfile, "plugin",
                dll::load_mode::append_decorations);

        _driver->initialize (option);

        LOG(INFO) << "A driver loadded : " << _driver->getcompany () << " "
            << _driver->getmodel() << " " << _driver->getversion ();
        _iosvc.run();
    }

    /**
     드라이버의 모델코드를 확인한다.
     @return 드라이버의 모델코드
    */
    int getmodelcode() {
        return _driver->getmodelcode();
    }

    /**
     드라이버의 API 버전을 확인한다.
     @return 드라이버의 API 버전
    */
    int getapispec() {
        return _driver->getapispec();
    }

    /**
     드라이버 가장 최근 업데이트된 시간을 확인한다. 
     @return 드라이버의 최근 업데이트 시각
    */
    time_t getlastupdated() {
        return _driver->getlastupdated();
    }

    /**
     드라이버의 내용이 업데이트되면 호출한다. 
     관리하고 있는 장비의 데이터가 변경되면 무조건 호출해 주어야 한다.
     현재는 최종업데이트 시간만을 관리한다.
    */
    void updated () {
        return _driver->updated();
    }

    /**
     드라이버 제작자가 부여하는 버전번호를 확인한다.
     컨버터에서는 해당 버전을 로깅용도로만 사용한다.
     문자열 비교를 통해 후순위가 더 높은 버전이 된다.
     @return 문자열 형식의 버전번호
    */
    string getversion () {
        return _driver->getversion();
    }

    /**
     드라이버 제작자가 부여하는 모델번호를 확인한다.
     컨버터에서는 모델코드만 확인하고, 모델번호에 대해서는 로깅용도로만 사용한다.
     @return 문자열 형식의 모델번호
    */
    string getmodel () {
        return _driver->getmodel();
    }

    /**
     드라이버 제조사명을 확인한다.
     컨버터에서는 제조사명을 로깅용도로만 사용한다.
     @return 문자열 형식의 제조사명
    */
    string getcompany () {
        return _driver->getcompany();
    }

    /**
     드라이버를 초기화 한다. 드라이버 동작을 위한 option 은 key-value 형식으로 전달된다.
     @param option 드라이버동작을 위한 옵션
     @return 초기화 성공 여부
    */
    bool initialize (CvtOption option) {
        return _driver->initialize(option);
    }

    /**
     드라이버를 종료한다.
     @return 종료 성공 여부
    */
    bool finalize () {
        return _driver->finalize();
    }

    /**
     드라이버간 상태교환을 하기전에 호출되는 메소드로 전처리를 수행한다.
     @return 전처리 성공 여부
    */
    bool preprocess () {
        return _driver->preprocess();
    }

    /**
     드라이버간 상태교환이 이루어진 이후에 호출되는 메소드로 후처리를 수행한다.
     @return 후처리 성공 여부
    */
    bool postprocess () {
        return _driver->postprocess();
    }

    /**
     드라이버가 관리하고 있는 장비의 포인터를 꺼내준다.
     모든 장비를 꺼내주지않고, 변경된 장비만을 꺼내주는 방식으로 효율을 높일 수 있다.
     @param index 얻고자 하는 장비의 인덱스 번호. 0에서 시작한다.
     @return 인덱스에 해당하는 장비의 포인터. NULL 이라면 이후에 장비가 없다는 의미이다.
    */
    void *getdevice(int index) {
        return _driver->getdevice(index);
    }

    /**
     드라이버가 관리하고 있는 장비의 값을 꺼낸다.
     센서라면 센싱값, 모터라면 위치값, 액츄에이터라면 온오프값
     @param index 얻고자 하는 장비의 인덱스 번호. 0에서 시작한다.
     @return 인덱스에 해당하는 장비의 값
    */
    double getvalue(void *pdevice) {
        double value = 0;
        CvtDevice *pdev = (CvtDevice *)pdevice;
        if (CvtMotor *pmotor= dynamic_cast<CvtMotor *>(pdev)) { // younger first
            value = pmotor->getcurrent();
        } else if (CvtActuator *pactuator = dynamic_cast<CvtActuator *>(pdev)) {
            value = pactuator->getonoff();
        } else if (CvtSensor *psensor = dynamic_cast<CvtSensor *>(pdev)) {
            value = psensor->readobservation();
        }
        return value;
    }

    /**
     전달된 장비의 정보를 획득한다. 
     다른 드라이버의 장비정보를 입력해주기 위해 컨버터가 호출한다.
     @param pdevice 다른 드라이버의 장비 포인터
     @return 성공여부. 관심이 없는 장비인 경우라도 문제가 없으면 true를 리턴한다.
    */
    bool sharedevice(CvtDevice *pdevice) {
        return _driver->sharedevice(pdevice);
    }

    /**
     다른 드라이버가 관리하고 있는 장비를 제어하고자 할때 명령을 전달한다.
     명령을 전달하지 않는 드라이버라면 그냥 NULL을 리턴하도록 만들면 된다.
     NULL이 나올때까지 반복적으로 호출한다.
     @return 명령의 포인터. NULL 이라면 이후에 명령이 없다는 의미이다.
    */
    CvtCommand *getcommand() {
        return _driver->getcommand();
    }
    
    /**
     다른 드라이버로부터 명령을 받아 처리한다.
     @param pcmd 명령에 대한 포인터
     @return 실제 명령의 처리 여부가 아니라 명령을 수신했는지 여부이다. 해당 명령을 실행할 장비가 없다면 false이다.
    */
    bool control(CvtCommand *pcmd) {
        return _driver->control(pcmd);
    }
};


} // namespace stdcvt

#endif

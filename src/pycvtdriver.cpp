/*
 Copyright © 2018 JiNong Inc. 
 All Rights Reserved.

 @file pycvtdriver.cpp
 @date 2018-04-05
 @author Kim, JoonYong <joonyong.jinong@gmail.com>

 This file is for cvtdriver wrapper for python use.
 refer from: https://gitlab.com/ebio-snu/stdcvt
*/

#include <pycvtdriver.h>

using namespace std;
using namespace pycvt;

namespace py = pybind11;

PYBIND11_MODULE(pycvt, m) {
    py::class_<PyCvtDriver>(m, "PyCvtDriver")
        .def(py::init<const std::string &>())
        .def("getmodelcode", &PyCvtDriver::getmodelcode)
        .def("getapispec", &PyCvtDriver::getapispec)
        .def("getlastupdated", &PyCvtDriver::getlastupdated)
        .def("updated", &PyCvtDriver::updated)
        .def("getversion", &PyCvtDriver::getversion)
        .def("getmodel", &PyCvtDriver::getmodel)
        .def("getcompany", &PyCvtDriver::getcompany)
        .def("initialize", &PyCvtDriver::initialize)
        .def("finalize", &PyCvtDriver::finalize)
        .def("preprocess", &PyCvtDriver::preprocess)
        .def("postprocess", &PyCvtDriver::postprocess)
        .def("getdevice", &PyCvtDriver::getdevice)
        .def("getvalue", &PyCvtDriver::getvalue)
        .def("sharedevice", &PyCvtDriver::sharedevice)
        .def("getcommand", &PyCvtDriver::getcommand)
        .def("control", &PyCvtDriver::control);
}
